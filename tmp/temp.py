import numpy as np

pred_boxes=[5,10,6,11]
true_boxes=[10,17,11,18]


c=[0,0,0,1,1]

max_iou_idx = np.argmax(np.array(c), axis=-1)

true_boxes=np.array(true_boxes)
pred_boxes=np.array(pred_boxes)

intersect_mins = np.maximum(pred_boxes[..., :2], true_boxes[..., :2])
intersect_maxs = np.minimum(pred_boxes[..., 2:], true_boxes[..., 2:])
intersect_wh = np.maximum(intersect_maxs - intersect_mins, 0.)


# shape: [N, V]
intersect_area = intersect_wh[..., 0] * intersect_wh[..., 1]
# shape: [N, 1, 2]
pred_box_wh = pred_boxes[..., 2:] - pred_boxes[..., :2]
# shape: [N, 1]
pred_box_area = pred_box_wh[..., 0] * pred_box_wh[..., 1]
# [1, V, 2]
true_boxes_wh = true_boxes[..., 2:] - true_boxes[..., :2]
# [1, V]
true_boxes_area = true_boxes_wh[..., 0] * true_boxes_wh[..., 1]

# shape: [N, V]
iou = intersect_area / (pred_box_area + true_boxes_area - intersect_area + 1e-10)

print("*"*40)
